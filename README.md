# shpool

`shpool` is a Python module that provides a simple thread pool-like interface for running shell scripts in parallel, either on a single machine or via a batch. For simplicity, results from the parallel jobs are processed sequentially in a single process. Therefore, `shpool` is inappropriate for parallel work that requires parallelized map and reduce operations, but it makes it easy to execute parallel jobs where post-processing/output collection is fast enough to happen serially.

The jobs can be written in any system that can be run as a subprocess. Input to the jobs is provided via standard input, and output is collected via standard output. Filesystem activity is also allowed, but requires more complex behavior by the controlling script.

The interface is encapsulated in the `ShellPool` class, which includes two basic methods, `run_one()` and `run_many()`, for submitting jobs to the pool.

## A simple example

Take a look at `run_job.py` in the `example` directory. It's a simple script written in Python that takes parameters via standard input in JSON format, and produces results via standard output, also in JSON format. It should look like this:

```{python}
#!/usr/bin/env python
import sys
import json

def run_job(job_id = None, alpha = None, beta = None, gamma = None):
    result = alpha * beta + gamma
    return {
        'job_id' : job_id,
        'result' : result
    }

if __name__ == '__main__':
    kwargs = json.load(sys.stdin)
    json.dump(run_simulation(**kwargs), sys.stdout)
    sys.stdout.write('\n')
```

You can see how it works by running it on its own. Run

```{sh}
$ ./run_job.py
```

and then type the following:
```{sh}
{"alpha" : 0.5, "beta" : 0.5, "gamma" : 0.5}
```
(or whatever values you like), and then press Control-D to indicate the end of input. It should spit out
```{sh}
{"result" : 0.75}
```
or the appropriate response corresponding to your input. Of course, a real job would do something more interesting. But the fact that it takes some data in via standard input and writes it out via standard output is all that matters.

Say you wanted to perform 1000 runs of this job, e.g. over 10 by 10 grid of values for `alpha` and `beta`, with 10 randomly chosen values of gamma for each grid location, and write information about the jobs, as well as results from each job, into a SQLite database. The `run_sweep.py` script shows how to do this using `shpool`.

Taking it piece by piece, at the top we have the Unix shebang indicating that this script should be run using Python:

```{python}
#!/usr/bin/env python
```

followed some import lines for libraries we'll use:

```{python}
import sqlite3
import json
import random
import os
import sys
```

and finally some lines to ensure that the script is run inside the `example` directory, and an import line to find `shpool` relative to the example script (which is necessary since it hasn't been properly packaged into an installed module yet):

```{python}
SCRIPT_DIR = os.path.dirname(__file__)
os.chdir(SCRIPT_DIR)
sys.path.append(os.path.join(SCRIPT_DIR, '..'))
import shpool
```

Next, `generate_jobs()` is a generator function (a function that can be iterated over) that writes information about each job into the database, and yields JSON-encoded data to be used as standard input for each job.


```{python}
def generate_jobs(db):
    job_id = 0
    for i in xrange(10):
        for j in xrange(10):
            for k in xrange(10):
                alpha = i * 0.1
                beta = j * 0.1
                gamma = random.random()
                
                db.execute(
                    'INSERT INTO job_info VALUES (?,?,?,?)',
                    [job_id, alpha, beta, gamma]
                )
                db.commit()
                
                yield json.dumps({
                    'alpha' : alpha,
                    'beta' : beta,
                    'gamma' : gamma
                })
                
                job_id += 1
```

Finally, the `main` section of the program starts by setting up the database, with one table holding information about job parameters, and the other table for results:

```{python}
if __name__ == '__main__':
    db = sqlite3.connect('results.sqlite')
    db.execute(
        'CREATE TABLE job_info (job_id INTEGER, alpha REAL, beta REAL, gamma REAL)'
    )
    db.execute(
        'CREATE TABLE results (job_id INTEGER, result REAL)'
    )
    db.commit()
```

Next, we create a `ShellPool` object, specifying the number of jobs to run in parallel, a temporary directory to hold job input and output files, and whether or not we want those files to remain in place after a job has completed:

```{python}
    pool = shpool.ShellPool(processes=4, tmp_dir='tmp_dir', keep_files=False)
```

To actually run the job, we provide a command for the shell pool and a call to the job generator function. The command is a list whose first item is the executable (`python`) and subsequent items are arguments to the executable (here, the absolute path to `run_job.py`).

```{python}
    call_id, n_jobs = pool.run_many(
        ['python', os.path.abspath('run_job.py')],
        generate_jobs(db)
    )
```

Next, we iterate through the results by repeatedly calling `pool.next_result()` and sequentially writing results to the database as jobs finish:
```{python}
    for i in xrange(n_jobs):
        call_id, chunk_id, job_id, job_dir, stdoutdata, exception = pool.next_result()
        result = json.loads(stdoutdata)['result']
        db.execute('INSERT INTO results VALUES (?,?)', [job_id, result])
        db.commit()
```
The `next_result()` command returns a number of items. We only make use of two: `job_id` is used to identify a particular job associated with a single request to the pool. `stdoutdata` is a byte array of the data written to standard output by the job, which we load and write to the database.

The `call_id` corresponds to the `call_id` returned from `run_many()`, and uniquely identifies each request given to the pool. The `chunk_id` relates to situations where you want to run multiple jobs in sequence in a single submission to the shell. If `keep_files` is set to `True`, `job_dir` points to the subdirectory in the temporary directory where the job ran, so you can process output besides standard output if you would like. Finally, if an exception is thrown by the submission machinery, it is provided in `exception`.

For completeness, we also have a couple of cleanup items:
```{python}
    pool.finish()
    db.close()
```

## Putting the example onto SLURM

To run this example on SLURM, we simply comment out the existing pool initialization and replace it with the commented-out SLURM version:

```{python}
    pool = shpool.ShellPool(
        processes = 20,
        shell = 'sbatch',
        chunksize = 20,
        shell = "sbatch",
        preamble = SLURM_PREAMBLE,
        tmp_dir = "tmp_dir",
        keep_Files = False
    )
```

noting that `SLURM_PREAMBLE` is defined earlier as

```{python}
SLURM_PREAMBLE = '''#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1000
#SBATCH --output=stdout_slurm.txt
#SBATCH --error=stderr_slurm.txt
#SBATCH --time=0:01:00
'''
```

Now, when jobs get run, the jobs simply get run using `sbatch` rather than `sh`. In fact, `shpool` knows nothing about SLURM, but because it monitors the filesystem for job completion, it works the same way whether it's running locally or on a cluster.

However, rather than simply running this script locally, we need to submit it as a job to SLURM, which will in turn ask the shell pool to submit more jobs to SLURM. We'll use the file `run_sweep.sbatch`, which contains resource requests for the babysitting SLURM job and simply runs `run_sweep.py`:

```{sh}
#!/bin/bash

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1000
#SBATCH --output=stdout_slurm.txt
#SBATCH --error=stderr_slurm.txt
#SBATCH --time=00:05:00
#SBATCH --mail-type=ALL

./run_sweep.py
```

Some more important notes:
1. The `processes` parameter determines how many jobs will be submitted at a time; it is up to the shell (e.g., SLURM) to decide how many will actually get run at a time. It's best to keep this less than the number of jobs you're allowed to run, since one job is taken up by the sweep itself.
2. The `chunksize` parameter specifies how many jobs will be run sequentially for each submission to SLURM. This is useful for making sure that every job is fairly long, in order to avoid overhead and per-submission charges.
3. The time requested from SLURM must be long enough for `chunksize` jobs to run, since they are all run using a single submission.

## Setting up a shell pool

Conceptually, a shell pool is a collection of parallel threads that can run commands through a specified shell, and can return results sequentially as they complete. If the specified shell is `sh` or another local shell, the commands run on the local machine. If the specified shell is, e.g., `sbatch` for the SLURM system, the commands are submitted to a batch system (cluster).

To set up a shell pool, you initialize a `ShellPool` object:

```{python}
import shpool

pool = shpool.ShellPool(
        processes=1,
        chunksize=1,
        polling_rate=1.0,
        wait_before_polling=True,
        shell="/bin/sh",
        preamble="#!/bin/sh",
        tmp_dir=None,
        keep_files=False
)
```

All constructor arguments are optional and have defaults as shown here. The meaning of the arguments is as follows:


### `shell`

The `shell` parameter specifies the shell used to submit jobs. Jobs are run by creating a file that consists of the `preamble` followed by a line executing a job proxy script, which is passed to the shell command as its first argument. When run, the job proxy runs individual jobs in the chunk (as specified by `chunksize`) sequentially and writes their standard output and completion status to the filesystem. All of these steps, including collecting the standard output data and returning it to the program, happen automatically.

The `shell` parameter can thus be any program that accepts a shell script-like file as its first argument, and will eventually cause the job proxy script to be executed in the same filesystem, for example the `sbatch` command from SLURM.

### `preamble`

The `preamble` is prepended to the script used to run the job proxy. When running locally, a standard Unix shebang line should suffice (`#!/bin/sh`). When running on a batch system such as SLURM, this is where to include directives to the batch system.

Importantly, you must specify a time limit for the job longer than the time it will take to execute `chunksize` jobs, not one job, since multiple jobs will be executed as part of the same submission.

For example, a SLURM preamble might look like this:

```{sh}
#!/bin/sh

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2000
#SBATCH --output=stdout_slurm.txt
#SBATCH --error=stderr_slurm.txt
#SBATCH --time=0:05:00
```

### `processes`

The `processes` argument determines how number of jobs will run in parallel. If you are running locally and this number is larger than the number of available cores, you will oversubscribe your machine; all jobs will run in parallel, but at less than full speed. If you are using a batch system, you should keep this number strictly less than the number of jobs you are allowed to run in parallel, since one job will typically be used to babysit the other jobs.

### `chunksize`

Particularly on batch systems---and particularly if the batch systems charge a minimum for each submission---you may want to run some number of jobs sequentially in a single submission. The `chunksize` parameter specifies how many jobs will be run sequentially as part of the same submission.

### `polling_rate`

The current version of `shpool` polls the filesystem to determine if jobs have completed, in order to avoid any dependencies on batch systems or file-system-notification libraries. To avoid synchronized polling by multiple processes, it happens probabilistically according to a Poisson process for each currently running submission. The `polling_rate` argument determines the mean rate, per second, of checking for job completion.

### `wait_before_polling`

If `wait_before_polling` is `True`, then polling for job completion will not begin until the submission script has returned. If running locally with `chunksize = 1`, or if running on a batch system that returns immediately after submission, `wait_before_polling = True` should be fine.

However, if you are using `chunksize > 1` with a local shell, for example to emulate the behavior of a batch system, you may want to use `wait_before_polling = False` so that results can be processed as they are generated.