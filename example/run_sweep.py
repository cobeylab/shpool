#!/usr/bin/env python

import sqlite3
import json
import random
import os
import sys
SCRIPT_DIR = os.path.dirname(__file__)
os.chdir(SCRIPT_DIR)
sys.path.append(os.path.join(SCRIPT_DIR, '..'))
import shpool

def generate_jobs(db):
    job_id = 0
    for i in xrange(10):
        for j in xrange(10):
            for k in xrange(10):
                alpha = i * 0.1
                beta = j * 0.1
                gamma = random.random()
                
                db.execute(
                    'INSERT INTO job_info VALUES (?,?,?,?)',
                    [job_id, alpha, beta, gamma]
                )
                db.commit()
                
                yield json.dumps({
                    'alpha' : alpha,
                    'beta' : beta,
                    'gamma' : gamma
                })
                
                job_id += 1

SLURM_PREAMBLE = '''#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1000
#SBATCH --output=stdout_slurm.txt
#SBATCH --error=stderr_slurm.txt
#SBATCH --time=0:01:00
'''

if __name__ == '__main__':
    db = sqlite3.connect('results.sqlite')
    db.execute(
        'CREATE TABLE job_info (job_id INTEGER, alpha REAL, beta REAL, gamma REAL)'
    )
    db.execute(
        'CREATE TABLE results (job_id INTEGER, result REAL)'
    )
    db.commit()

    pool = shpool.ShellPool(processes = 4, tmp_dir = 'tmp_dir', keep_files = False)
#    pool = shpool.ShellPool(
#        processes = 20,
#        shell = 'sbatch',
#        preamble = SLURM_PREAMBLE,
#        chunksize = 20,
#        tmp_dir = "tmp_dir",
#        keep_files = False
#    )
    
    call_id, n_jobs = pool.run_many(
        ['python', os.path.abspath('run_job.py')],
        generate_jobs(db)
    )

    for i in xrange(n_jobs):
        call_id, chunk_id, job_id, job_dir, stdoutdata, exception = pool.next_result()
        result = json.loads(stdoutdata)['result']
        db.execute('INSERT INTO results VALUES (?,?)', [job_id, result])
        db.commit()
    
    
    pool.finish()
    db.close()
