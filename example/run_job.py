#!/usr/bin/env python

import sys
import json

def run_job(alpha = None, beta = None, gamma = None):
    result = alpha * beta + gamma
    return {
        'result' : result
    }

if __name__ == '__main__':
    kwargs = json.load(sys.stdin)
    json.dump(run_job(**kwargs), sys.stdout)
    sys.stdout.write('\n')
