#!/usr/bin/env python

import os
import sys
import json
import subprocess

def main():
    chunk_filename = sys.argv[1]
    
    with open(chunk_filename) as f:
        chunk_spec = json.load(f)
    run_command = chunk_spec['run_command']
    call_id = chunk_spec['call_id']
    chunk_id = chunk_spec['chunk_id']
    job_ids = chunk_spec['job_ids']
    
    for job_id in job_ids:
        run_job(run_command, job_id)

def run_job(run_command, job_id):
    sys.stderr.write('job_proxy running job_id {0}...\n'.format(job_id))
    
    job_dir = str(job_id)
    stdinfile = open(os.path.join(job_dir, 'stdin'), 'rb')
    stdoutfile = open(os.path.join(job_dir, 'stdout'), 'wb')
    stderrfile = open(os.path.join(job_dir, 'stderr'), 'wb')
    proc = subprocess.Popen(
        run_command,
        cwd=job_dir, stdin=stdinfile, stdout=stdoutfile, stderr=stderrfile
    )
    proc.wait()
    stdinfile.close()
    stdoutfile.close()
    stderrfile.close()
    
    with open(os.path.join(job_dir, 'complete'), 'w') as f:
        pass

if __name__ == '__main__':
    main()
