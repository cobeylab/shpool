import os
import sys
import json
import time
import random
import multiprocessing
import subprocess
import tempfile
import subprocess
import shutil

def init_run_chunk(shell, wait_before_polling, polling_rate, tmp_dir, keep_files, queue):
    run_chunk.shell = shell
    run_chunk.wait_before_polling = wait_before_polling
    run_chunk.polling_rate = polling_rate
    run_chunk.tmp_dir = tmp_dir
    run_chunk.keep_files = keep_files
    run_chunk.queue = queue

def run_chunk(call_id, chunk_id):
    self = run_chunk
    
    chunk_dir = os.path.join(self.tmp_dir, str(call_id), str(chunk_id))
    chunk_script = os.path.abspath(os.path.join(self.tmp_dir, 'chunk_script'))
    
    proc = None
    exception = None
    try:
        proc = subprocess.Popen(
            [self.shell, chunk_script],
            cwd=chunk_dir
        )
    except Exception as e:
        exception = e
    
    chunk_spec_filename = os.path.join(chunk_dir, 'chunk_spec.json')
    with open(chunk_spec_filename) as f:
        job_ids = json.load(f)['job_ids']
    
    if exception is None and self.wait_before_polling:
        proc.wait()
    
    for job_id in job_ids:
        job_dir = os.path.join(chunk_dir, str(job_id))
        
        if exception is None:
            complete_filename = os.path.join(job_dir, 'complete')
            while not os.path.exists(complete_filename):
                time.sleep(random.expovariate(self.polling_rate))
            
            with open(os.path.join(job_dir ,'stdout'), 'rb') as f:
                stdoutdata = f.read()
            
            with open(os.path.join(job_dir ,'stderr'), 'rb') as f:
                stderrdata = f.read()
        else:
            stdoutdata = None
            stderrdata = None
        
        self.queue.put([call_id, chunk_id, job_id, job_dir if self.keep_files else None, stdoutdata, exception])
        if not self.keep_files:
            shutil.rmtree(job_dir)
    
    if exception is None and not self.wait_before_polling:
        proc.wait()
    
    if not self.keep_files:
        shutil.rmtree(chunk_dir)

class ShellPool:
    def __init__(
        self,
        shell="/bin/sh",
        preamble="#!/bin/sh",
        processes=1,
        chunksize=1,
        polling_rate=1.0,
        wait_before_polling=True,
        tmp_dir=None,
        keep_files=False
    ):
        self.processes = processes
        self.chunksize = chunksize
        self.polling_rate = polling_rate
        self.shell = shell
        self.preamble = preamble
        if tmp_dir is None:
            tmp_dir = tempfile.mkdtemp()
        else:
            try:
                os.makedirs(tmp_dir)
            except:
                pass
        self.tmp_dir = tmp_dir
        job_proxy_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'job_proxy.py'))
        chunk_script_filename = os.path.join(tmp_dir, 'chunk_script')
        with open(chunk_script_filename, 'w') as f:
            f.write(self.preamble)
            f.write('\n\n')
            f.write('{0} chunk_spec.json\n'.format(job_proxy_path))
        
        self.keep_files = keep_files
        
        sys.stderr.write('tmp_dir: {0}\n'.format(self.tmp_dir))
        
        self.queue = multiprocessing.Queue()
        self.pool = multiprocessing.Pool(
            processes=processes, 
            initializer=init_run_chunk,
            initargs=[shell, wait_before_polling, polling_rate, tmp_dir, keep_files, self.queue]
        )
        
        self.next_call_id = 0
        
        self.done = False
    
    def apply(self, run_command, args=None, kwds=None):
        pass
    
    def get_call_id(self):
        call_id = self.next_call_id
        self.next_call_id += 1
        return call_id
    
    def get_chunk_dir(self, call_id, chunk_id):
        return os.path.join(self.tmp_dir, str(call_id), str(chunk_id))
    
    def get_chunk_spec_filename(self, call_id, chunk_id):
        return os.path.join(self.get_chunk_dir(call_id, chunk_id), 'chunk_spec.json')
    
    def make_chunk_dir(self, call_id, chunk_id):
        chunk_dir = self.get_chunk_dir(call_id, chunk_id)
        try:
            os.makedirs(chunk_dir)
        except:
            pass
        return chunk_dir
    
    def get_job_dir(self, call_id, chunk_id, job_id):
        return os.path.join(self.get_chunk_dir(call_id, chunk_id), str(job_id))
    
    def set_up_job(self, call_id, chunk_id, job_id, stdindata):
        job_dir = self.get_job_dir(call_id, chunk_id, job_id)
        os.makedirs(job_dir)
        stdin_filename = os.path.join(job_dir, 'stdin')
        with open(stdin_filename, 'wb') as f:
            f.write(stdindata)
            f.write('\n')
    
    def read_job_stdout(self, call_id, chunk_id, job_id):
        job_dir = self.get_job_dir(call_id, chunk_id, job_id)
        stdout_filename = os.path.join(job_dir, 'stdout')
        with open(stdout_filename, 'rb') as f:
            return f.read()
    
    def read_job_ids(self, call_id, chunk_id):
        chunk_spec_filename = self.get_chunk_spec_filename(call_id, chunk_id)
        with open(chunk_spec_filename) as f:
            return json.load(f)['job_ids']
    
    def submit_chunk(self, run_command, call_id, chunk_id, job_ids):
        chunk_spec_filename = self.get_chunk_spec_filename(call_id, chunk_id)
        with open(chunk_spec_filename, 'w') as f:
            json.dump({
                'run_command' : run_command,
                'call_id' : call_id,
                'chunk_id' : chunk_id,
                'job_ids' : job_ids
            }, f)
            f.write('\n')
        
        self.pool.apply_async(run_chunk, [call_id, chunk_id])
    
    def run_one(self, run_command, stdindata):
        call_id = self.get_call_id()
        
        self.set_up_job(call_id, 0, 0, stdindata)
        self.submit_chunk(run_command, call_id, 0, [0])
        
        return call_id
    
    def run_many(self, run_command, iterable):
        call_id = self.get_call_id()
        
        n_jobs = 0
        
        chunk_job_ids = []
        chunk_id = 0
        for job_id, stdindata in enumerate(iterable):
            n_jobs += 1
            chunk_dir = self.make_chunk_dir(call_id, chunk_id)
            self.set_up_job(call_id, chunk_id, job_id, stdindata)
            chunk_job_ids.append(job_id)
            if len(chunk_job_ids) == self.chunksize:
                self.submit_chunk(run_command, call_id, chunk_id, chunk_job_ids)
                chunk_id += 1
                chunk_job_ids = []
        if len(chunk_job_ids) > 0:
            self.submit_chunk(run_command, call_id, chunk_id, chunk_job_ids)
        
        return call_id, n_jobs
    
    def next_result(self):
        return self.queue.get()
    
    def finish(self):
        assert not self.done
        self.pool.close()
        self.pool.join()
        
        if not self.keep_files:
            shutil.rmtree(self.tmp_dir)
    
    def get_temporary_directory(self):
        return self.tmp_dir
